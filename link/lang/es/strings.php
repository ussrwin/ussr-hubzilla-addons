<?php

App::$strings["Link on Hub"] = "Enlazar con la entrada en el hub";
App::$strings["Show \"Link on Hub\" in the dropdown menu"] = "Mostrar \"Enlazar con la entrada en el hub\" en el menú desplegable";
App::$strings["Hide side panels on the link page"] = "Ocultar los paneles laterales en la página de enlaces";