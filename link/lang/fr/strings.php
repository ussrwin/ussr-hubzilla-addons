<?php

App::$strings["Link on Hub"] = "Lien sur le Hub";
App::$strings["Show \"Link on Hub\" in the dropdown menu"] = "Afficher \"Lien sur le Hub\" dans le menu déroulant";
App::$strings["Hide side panels on the link page"] = "Masquer les panneaux latéraux sur la page de liens";
